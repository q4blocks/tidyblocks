'use strict';

import express from 'express';
import apiRouter from './routes/snippets';
import projectRouter from './routes/projects';
import fetchProjectRouter from './routes/fetch-projects';
import { initTestDatastore } from './lib/datastore';
import "babel-core/register";
import "babel-polyfill";

var startupTime;
const app = express();
app.set('json spaces', 2);

app.get('/', (req, res) => {
  let status = {
    version: process.env.GAE_VERSION || 'local',
    startup_time: startupTime
  }
  res.status(200).json(status);
});

app.use('/editor', express.static('build'));
app.use('/snippets', apiRouter);
app.use('/projects', projectRouter);
app.use('/fetch-projects', fetchProjectRouter);

const server = app.listen(process.env.PORT || 8080, () => {
  startupTime = new Date(Date.now()).toLocaleString();
  if (process.env.NODE_ENV === 'production') {
    console.log("production mode");
  } else {
    console.log("configure for development mode");
    initTestDatastore().catch(err => {
      console.log('cannot initialize datastore for testing: ', err);
    })
  }

});

export default app;