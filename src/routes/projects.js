'use strict';

import express from 'express';
import {extractXml} from '../lib/xmlutil';

var router = express.Router();

router.get('/:projectId/xml', function (req, res){
    let projectId = req.params['projectId'];
    extractXml(projectId, res);
});


export default router;