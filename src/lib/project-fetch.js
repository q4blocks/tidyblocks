/**
 * fetching project information from official Scratch website
 */

import fetch from 'node-fetch';
import "babel-core/register";
import "babel-polyfill";

const getProjectQueryUrl = function (offset, mode) {
    return `https://api.scratch.mit.edu/explore/projects?limit=10&offset=${offset}&language=en&mode=${mode}&q=*`;
};

// https://api.scratch.mit.edu/explore/projects?limit=16&offset=0&language=en&mode=recent&q=*

const retrieveProjects = async function(projectEntryStartOffset, numProjects, mode) {
    const limit = 10; // 10 projects per request
    let projectList = [];
    let projectCount = 0;
    //  offsets calculation
    let endOffset = Math.ceil(numProjects/limit);
   
    for(let offset = 0; offset < endOffset; offset++  ){
        let adjustedOffset = (projectEntryStartOffset+offset*limit);
        const projects = await fetch(getProjectQueryUrl(adjustedOffset,mode)).then((response)=> response.json());
        for (const p of projects) {
            //use scratch api to retrieve project info (the project info in the list has missing info --zero remix counts)
            let projectInfo = await fetch("https://api.scratch.mit.edu/projects/"+p.id).then((response)=> response.json());
            
            if (projectCount >= numProjects) {
                return projectList;
            }
            projectList.push(projectInfo);
            projectCount++;
        }
    }
    console.log('retrieving successfully');
    return projectList;
}



export {retrieveProjects};