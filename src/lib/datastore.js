
import { Datastore } from '@google-cloud/datastore';
import "babel-core/register";
import "babel-polyfill";

const datastore = new Datastore({ projectId: 'tidyblocks' });
const confKey = datastore.key(['Config', 'es-password']);

const retrievePassword = async function () {
    const res = await datastore.get(confKey);
    const [conf] = res;
    return conf.conf_value;
}

const initTestDatastore = async function () {
  const kind = 'Config';
  const name = 'es-password';
  const confKey = datastore.key([kind, name]);
  const conf = {
    key: confKey,
    data: {
      conf_value: "BcVKjdUmJh8L"
    },
  };
  await datastore.save(conf);
}


export { retrievePassword, initTestDatastore};