import elasticsearch from 'elasticsearch';

const es_endpoint = '104.155.143.58/elasticsearch';

const getEsData = async function (es_pwd) {
    const client = await getEsClient(es_pwd);
    const snippets = await fetchAllSnippets(client);
    return snippets;
};

const fetchAllSnippets = async function (client) {
    let resp = await client.search({
        index: 'snippet',
        type: 'procedure',
        body: {
            query: {
                match_all: {}
            }
        }
    });
    let data = resp ? resp.hits.hits : [];
    console.debug(`Snippets fetched. Count: ${data.length}`);
    return data;
};

const getEsClient = async function (es_pwd) {
    const client = new elasticsearch.Client({
        host: es_endpoint,
        httpAuth: 'user:' + es_pwd,
        log: 'error'
    });

    client.ping({
        requestTimeout: 30000,
    }, function (error) {
        if (error) {
            console.error('Elasticsearch cluster is down!');
        } else {
            console.log('Elasticsearch is running...');
        }
    });

    return client;
};

export { getEsClient, getEsData };