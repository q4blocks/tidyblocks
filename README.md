
# Tidyblocks

Serving a modified version of Scratch 3.0 with code quality improvement support.
* this nodejs project uses Express to serve static files from build directory
* place the scratch-gui/build in the top level of this project (run `npm run build` in scratch-gui)

```
# setup
npm install

# running locally
npm run start

# running in a dev mode with reload when `/src` is modified
npm run dev

# to debug the app
node --inspect dist/index.js


# deploy to Google App Engine
npm run deploy

```

--------------------
# fetch projects
http://localhost:8080/fetch-projects?mode=recent&num_projects=100